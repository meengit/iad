# Daten analysieren


## Der User


<iframe width="1120" height="560" src="https://www.youtube.com/embed/OlMkf1FU5aM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<small>Werbevideo Lean Libray (Universitätsbibliothek Bern, 2019)</small>


## Informationsarchitektur


<img src="https://media.kulturbanause.de/2019/04/hierarchische-informationsarchitektur.svg" alt="Hierarchische Informationsarchitektur" style="width:auto;height:480px;" >

<small>Hierarchische Informationsarchitektur (kulturbanause Team, 2021)</small>


<img src="https://media.kulturbanause.de/2019/04/netzfoermige-informationsarchitektur.svg" alt="Netzartige Informationsarchitektur" style="width:auto;height:480px;" >

<small>Netzartige Informationsarchitektur (kulturbanause Team, 2021)</small>


## Daten beschaffen

Was tun, wenn noch keine Daten verfügbar sind?


* Datenquellen mit ähnlichen Datenstrukturen suchen
  * [opendata.swiss](https://opendata.swiss)
  * [Schweizerisches Bundesamt für Statistik](https://www.bfs.admin.ch)
  * [KOF Konjunkturforschungsstelle](https://kof.ethz.ch/)
  * [GFS Bern](https://www.gfsbern.ch)
  * ...
* Fake-Daten generieren:
  * [faker.js](https://github.com/marak/Faker.js/)
  * [mockaroo](https://www.mockaroo.com/)
  * [20 Resources for generating fake and mock data ](https://dev.to/iainfreestone/20-resources-for-generating-fake-and-mock-data-55g1)


## Daten auswerten


* Struktur erkennen
* ***Zusammenhänge erkennen***


### Gephi

<img src="./images/analyse/gephyweb.png" alt="Gephi, visuelle Analysen" style="width:auto;height:480px;" >

<small>Daten visuell analysieren mit [gephi.org](https://gephi.org/)</small>


#### Alternativen

* [graphviz](https://www.graphviz.org/)
* [socnetv](https://socnetv.org/)
* [geogebra](https://www.geogebra.org/)
* [nodebox](https://www.nodebox.net/)
