# Von der Datenbank zur Ausgabe


## Andreas Eberhard

* Principal Engineer (Full-Stack IIoT), Head of IT
* Master of Arts in integrativer Gestaltung
* Mitglied der [Free Software Foundation](https://www.fsf.org)
* Mozilla [Contributor](https://www.mozilla.org/en-US/contribute/)


## Agenda

* Datenbanken, eine Übersicht
* Daten analysieren in einer praktischen Herangehensweise
* Eigenes Projekt realisieren


## Prüfung

* Unser Fach wird benotet
* Die «Prüfung» ist eine Gruppen- oder Einzelarbeit
* Wer engagiert dem Unterricht folgt, kann auch eine gute Prüfung ablegen


### Gewichtung der Prüfung

Die Note der Prüfung wird nach dem Lektionenschlüssel gewichtet.


### Prüfungsaufgabe

* Analysieren sie die Webseite [corona-data.ch](https://corona-data.ch/)
  * Was fällt ihnen persönlich auf? Wählen sie drei Punkte.
  * Welche Informationsarchitektur erkennen sie in den Daten? Wählen sie eine Quelle aus und beschreiben sie die Informationsarchitektur, die sie sehen.
* Präsentieren und begründen sie stichhaltig (mit Referenzen oder Literatur) 2–3 Punkte, die sie ändern würden
* Erarbeiten sie:
  * 1–3 Visuals, in denen sie eine Verbesserung des UI/UX aufzeigen ***ODER*** (für Einzelarbeiten)/***UND*** (für Gruppenarbeiten)
  * Erarbeiten sie 1–2 grundlegend neue Visuals (Entwürfe) ab den bestehenden Daten
* Extrapunkt: Erklären sie ihre Visuals oder Entwürfe in einer Sprachaufnahme ohne visuelle Hilfsmittel


### Abgabeformat

* Analysen: Einfaches Word- oder Textdokument, max. 1 A4 Seite Text (Grafiken/Scribbles sind wünscheswert, werden aber nicht zum Seitenumfang gezählt)
* Präsentation «Verbesserungen»: Präsentation als PDF
* Visuals: Offene Daten inklusive allfälliger Notizen


### Abgrenzung

Sollte es im Unterricht zu einem zeitlichen Engpass kommen, werden die Prüfungsaufgaben automatisch von unten nach oben gekürzt.
