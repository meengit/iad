# Übungen


## Gruppenarbeit wichtige Datenbanktypen/Unternehmen


* [Exasol](https://www.exasol.com/en/)
* [Google Spanner](https://en.wikipedia.org/wiki/Spanner_(database))
* [IBM DB2](https://www.ibm.com/analytics/db2)
* [MongoDB](https://www.mongodb.com/)
* [MySQL und MariaDB](https://en.wikipedia.org/wiki/MariaDB)
* [Oracle (DB 19c)](https://www.oracle.com/database/technologies/)
* [Postgresql](https://www.postgresql.org/)
* [Redis](https://redis.io/)
* [SQLite](https://www.sqlite.org/index.html?)


### Aufgabe

* Geschichte der Datenbank
* Datenbanktyp (SQL, NoSQL, andere)
* Vor- und Nachteile (was wird empfohlen)

<small>ca. 1 Lektion für Recherche und kurze Präsentation</small>


## Normalisierung


Ziel: Konsistenzerhöhung durch Redundanzvermeidung


### Normalisierung Stufe 1

*Jedes Attribut der Relation muss einen atomaren Wertebereich haben, und die Relation muss frei von Wiederholungsgruppen sein.*


![Normalisierung Stufe 1, falsch](images/datenbanken/normalization-1.wrong.png)


![Normalisierung Stufe 1, richtig](images/datenbanken/normalization-1.ok.png)


### Normalisierung Stufe 2

*Eine Relation ist genau dann in der zweiten Normalform, wenn die erste Normalform vorliegt und kein Nichtprimärattribut (Attribut, das nicht Teil eines Schlüsselkandidaten ist) funktional von einer echten Teilmenge eines Schlüsselkandidaten abhängt.*


![Normalisierung Stufe 2, falsch](images/datenbanken/normalization-2.wrong.png)


![Normalisierung Stufe 2, richtig](images/datenbanken/normalization-2.ok.png)


### Normalisierung Stufe 3

*Die dritte Normalform ist genau dann erreicht, wenn sich das Relationenschema in der 2NF befindet, und kein Nichtschlüsselattribut von einem Schlüsselkandidaten transitiv abhängt.*


![Normalisierung Stufe 3, falsch](images/datenbanken/normalization-3.wrong.png)


![Normalisierung Stufe 3, richtig](images/datenbanken/normalization-3.ok.png)


## Einführung in Gephi

Tutorial basierend auf <https://www.youtube.com/watch?app=desktop&v=371n3Ye9vVo> «Updated Gephi Quick Start Tutorial for v 0.9» (jengolbeck, 2018)


### Gephi herunterladen & installieren

<https://gephi.org/>


### Demo-Projekt herunterladen

<https://github.com/gephi/gephi/wiki/Datasets>, GML file «Les Miserables»


### Hands on…
