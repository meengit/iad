# Checkliste DB, Analyse, Design


* [ ] Woher kommen die Daten?
* [ ] Welches DBMS wird verwendet?
* [ ] Hängt ein Data Warehouse o. Ä. dazwischen?
* [ ] Wie oft werden die Daten aktualisiert? Gibt es Latenzen?
* [ ] Welche Fehler bei der Datenübertragung sind möglich?
* [ ] Hab ich die wichtigsten Fehler in meinem Design berücksichtigt?
