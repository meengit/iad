# Datenbanken


## Ein Blick in die Geschichte


<iframe width="1120" height="560" src="https://www.youtube.com/embed/KG-mqHoXOXY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<small>The History of Databases (Computer History Museum, 2018)</small>


## Datenbanktypen


![Datenbank Typen](./images/datenbanken/db-history.png)


## Datenbanktypen (zusammengefasst)

* [Relational database](https://en.wikipedia.org/wiki/Relational_database)
  * [SQL](http://www.sqlcourse.com/intro.html)
* [NoSQL](https://cdn.ttgtmedia.com/rms/onlineImages/data_management-nosql.png)
  * [Document-oriented database](https://en.wikipedia.org/wiki/Document-oriented_database)
* [NewSQL](https://en.wikipedia.org/wiki/NewSQL)


## Datenbank-Management-System (DBMS)


<img src="images/datenbanken/dbms.png" alt="DBMS" style="width:auto;height:480px;" >

<small>Aus «DBMS: An Intro to Database Management Systems» (Raza, 2018)</small>


## SQL


<img src="images/datenbanken/sqltables.png" alt="SQL Tables" style="width:auto;height:480px;" >

<small>Aus «Table Relationships» (LaunchSchool, o. J.)


### Beziehungen


#### 1:1 – eins zu eins; 

*Jeder Datensatz in Tabelle A ist genau einem Datensatz in Tabelle B zugeordnet.*


#### 1:n – eins zu viele;

*Der häufigste Beziehungstyp in relationalen Datenbanken; In einer 1:n-Beziehung können einem Datensatz in Tabelle A mehrere passende Datensätze in Tabelle B zugeordnet sein – aber einem Datensatz in Tabelle B ist nie mehr als ein Datensatz in Tabelle A zugeordnet.*


#### m:n – viele zu viele;

*Jedem Datensatz in Tabelle A können mehrere passende Datensätze in Tabelle B zugeordnet sein und umgekehrt.*


### Die wichtigsten Datentypen

![Datentypen](images/datenbanken/datatypes.png)


## NoSQL

![NoSQL Types](images/datenbanken/nosql.jpg)

<small>Aus «Cloud NoSQL for SQL Professionals» (Langit, 2020)


## Datawarehouse-Architektur

Ein Beispiel aus der Gegenwart.


<iframe width="1120" height="630" src="https://www.youtube.com/embed/avL5EnFTKzk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<small>Data Warehouse Architektur (Grosser, 2017)</small>


## Das ANSI Modell

<img src="https://upload.wikimedia.org/wikipedia/commons/0/0e/Drei-Ebenen-Schema-Architektur.svg" alt="ANSI SPARC Modell" style="width:auto;height:480px;" >

 <small>Drei-Ebenen-Schema-Architektur nach ANSI SPARC (Wikipedia Community, 2020)</small>


## Architektur für Datenbanken und Applikationsserver


<img src="https://miro.medium.com/max/600/1*Ef0mGtrajWqqwlu8LM9pNw.png" alt="1-tier" style="width:auto;height:480px;" >

<small>Architektur 1-tier (Ahmed, 2017)</small>


<img src="https://miro.medium.com/max/600/1*z76hr15cj2CVVlWDNITV2A.png" alt="2-tier" style="width:auto;height:480px;" >

<small>Architektur 2-tier (Ahmed, 2017)</small>


<img src="https://miro.medium.com/max/1222/1*2V5V379CNVAsjpBiep0mjQ.png" alt="2-tier" style="width:auto;height:480px;" >

<small>Architektur 3-tier (Ahmed, 2017)</small>


## CRUD

`CREATE`, `READ`, `UPDATE`, and `DELETE`

<small>(Kersken, 2019, S. 1122)</small>


**Principles of CRUD (nach Watts, 2018):**

* `CREATE` procedures generate new records via INSERT statements.
* `READ` procedures reads the data based on input parameters. Similarly, RETRIEVE procedures grab records based on input parameters.
* `UPDATE` procedures modify records without overwriting them.
* `DELETE` procedures delete where specified.


## REST

Representational State Transfer


**There are six guiding constraints of REST (nach Watts, 2018):**

* Client-server mandata
* Statelessness
* Cache
* Interface/uniform contract
* Layered system
* Code-on-demand (optional)
