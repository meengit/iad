const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

console.info('---> NODE ENV: ' + process.env.NODE_ENV);
console.info('---> Working Dircectory: ' + path.resolve(process.cwd()));
console.info('---> Source Dircectory: ' + path.resolve(process.cwd(), 'src'));

module.exports = {
  entry: './src/js/index.js',
  mode: process.env.NODE_ENV || 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    hot: true,
    compress: true,
    port: 9000,
    watchFiles: [path.resolve(process.cwd(), 'src/**/*')],
    liveReload: true,
    devMiddleware: {
      serverSideRender: true,
      writeToDisk: true,
    },
  },
  module: {
    rules: [
      {
        test: /\.(ttf|woff|eot)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: './src/slides', to: './slides' },
        { from: './src/slides/images', to: './images' },
      ],
    }),
    new HtmlWebpackPlugin({ template: './src/index.html' }),
  ],
};
