# Von der Datenbank zur Anzeige

Unterrichtsmaterialien für HF IAD2017 und IAD2019 an der SFGZ.

* Einführung IAD2017: [`./IAD2017/README.md`](./IAD2017/README.md)
* Präsentation IAD2019: [`./IAD2019/dist/index.html`](./IAD2019/dist/index.html)

## Urheberrecht & Lizenzen

Der Programmcode steht unter der [`MIT-Lizenz`](./LICENSE). Die Texte und Bilder stehen unter der [Creative Commons BY NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode) Lizenz.

Für den Programmcode und die Inhalte von Dritten gelten deren Lizenen.
